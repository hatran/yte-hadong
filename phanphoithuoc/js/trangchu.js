$(document).ready(function() {
    function resizeReportContainer() {
        $('.report-container').each(function(index) {
            var itemsCount = $(this).children().length;
            if (itemsCount) {
                var itemHeight = $(this).height() / $(this).children().length;
                
                console.log(itemHeight);
                $(this).children().each(function() {
                    $(this).height(itemHeight-16);

                });
                // $('#chart-thuoctonkho').height(itemHeight / 1.5);
                // $('#chart-thuoctonkho').width($(this).width() / 1.2);
                // $('#chart-thuocchamtieuthu').height(itemHeight / 1.5);

                $('#chart-thuoctonkho').css("transform","scale("+ (itemHeight / 160 * 0.65) +")")
                // $('#chart-thuocchamtieuthu').css("transform","scale("+ (itemHeight / 160 * 0.65) +")")
                $('#chart-thuocchamtieuthu').css("transform","scale(0.7)")
            }
          });
        
    }
    
    $(window).resize(function() {
        resizeReportContainer();
    });
    resizeReportContainer();
    /*var ttkho = new Chart(document.getElementById('chart-thuoctonkho').getContext('2d'), {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    8, 32
                ],
                backgroundColor: [
                    '#44ff44',
                    '#f1f1f1'
                ],
                borderWidth: [0,0],
                //label: 'Dataset 1'

            }],
            labels: [
                'Hiện  tồn',
                'Tổng 2.968 Loại Thuốc'
            ]
        },
        
        options: {
            circumference: Math.PI,
            rotation: -Math.PI,
            responsive: true,
            legend: {
                position: 'top',
                labels: {
                    fontColor: "white",
                    fontSize: 16
                }
            },
            multiTooltipTemplate: "<%%=datasetLabel%> : <%%= value%> ",
            animation: {
                animateRotate: true
            },
            maintainAspectRatio: true,
        }
    });*/
    var chamtt = new Chart(document.getElementById('chart-thuocchamtieuthu').getContext('2d'), {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    4, 16
                ],
                backgroundColor: [
                    'red',
                    'rgb(14, 255, 22)'
                ],
                borderWidth: [0,0],
                //label: 'Dataset 1'

            }],
            labels: [
                'Thuốc chậm tiêu thụ',
                'Tổng 2.968 loại thuốc'
            ]
        },
        
        options: {
            circumference: Math.PI,
            rotation: -Math.PI,
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'top',
                labels: {
                    fontColor: "red",
                    fontSize: 16
                }
            },
            multiTooltipTemplate: "<%%=datasetLabel%> : <%%= value%> ",
            animation: {
                animateRotate: true
            },
            maintainAspectRatio: true,
        }
    });
    

})
